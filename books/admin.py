from django.contrib import admin
from books.models import Book, Magazine, Genre, Issue, BookReview, Author

# Register your models here.

class BookAdmin(admin.ModelAdmin):
    pass

class MagazineAdmin(admin.ModelAdmin):
    pass

class GenreAdmin(admin.ModelAdmin):
    pass

class IssueAdmin(admin.ModelAdmin):
    pass

admin.site.register(Book, BookAdmin)
admin.site.register(BookReview)
admin.site.register(Magazine, MagazineAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Issue, IssueAdmin)
admin.site.register(Author, IssueAdmin)
