from django import forms
from books.models import Book, Magazine

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
        "image",
        "title",
        "author",
        "description",
        "isbn",
        "pages",
        "year_published",
        "inprint"
        ]

class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
        "image",
        "title",
        "description",
        # "genre",
        "cycle",
        ]


