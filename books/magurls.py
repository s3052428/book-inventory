from django.urls import path
from books.views import(
    # delete_magazine,
    show_magazines,
    create_magazine,
    show_magazine,
    delete_magazine,
    update_magazine,
    show_genre,
    list_reviews,
)
# we need to import the views file to reference the functions defined in views.py

urlpatterns = [
    # Call function "book_list()" to get the html render
    # This is path /books/
    path('', show_magazines, name="magazine_detail"),
    path('create/', create_magazine, name="create_magazine"),
    path('<int:pk>/', show_magazine, name="magazine_detail"),
    path('<int:pk>/delete', delete_magazine, name="delete_magazine"),
    path('<int:pk>/update', update_magazine, name="update_magazine"),
    path('<str:genre_name>/', show_genre, name="show_genre"),
]
