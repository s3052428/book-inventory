from email.mime import image
from itertools import cycle
from unittest.util import _MAX_LENGTH
from django.db import models
# Create your models here.
from django.contrib.auth.models import User

class Book(models.Model):

    title = models.CharField(max_length=200, unique=True)
    author = models.ManyToManyField("Author", related_name="books")  
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    inprint = models.BooleanField(null=True)
    
    def __str__(self):
        return self.title + " by " + str(self.author.first())


class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)
    def __str__(self):
        return self.name



class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()
    reviewer = models.ForeignKey(User, related_name="reviews_from_bookreview_class", on_delete=models.CASCADE)
    def __str__(self):
        return self.text


class Magazine(models.Model):
    class CyclePeriod(models.TextChoices):
        WEEKLY = 'Weekly', ('Weekly')
        MONTHKY = 'Monthly', ('Monthly')
        QUARTELY = 'Quartely', ('Quartely')
        YEARLY = 'Yearly', ('Yearly')

    # This is one to many (one genre many magazines)
    # genre = models.ForeignKey("Genre", on_delete = models.CASCADE, blank=True, null=True, related_name="magazines") 
     
    # this is many to many (magazine can have many genres, and vice versa)
     
     
    # on_delete = models.CASCADE --> If you delete the genre, delete all connected magazines
    # The actual purpose of the ForeignKey Field is that It connects the Book model with the Author model.

    creater = models.ForeignKey(User, related_name="magazines", on_delete=models.CASCADE)
    # genre = models.ManyToManyField("Genre", related_name="magazines")
    title = models.CharField(max_length=200, unique=True)
    # cycle = models.CharField(max_length=10)
    cycle = models.CharField(
        max_length=10,
        choices=CyclePeriod.choices,
        default=CyclePeriod.QUARTELY,
    )

    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)


    def __str__(self):
        return self.title 


class Genre(models.Model):
    name = models.CharField(max_length = 50)
    magazine = models.ManyToManyField("Magazine", related_name="genres")
    def __str__(self):
        return self.name


class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    image = models.URLField(null=True, blank=True)
    title = models.CharField(max_length=200, unique=True)
    issue = date = models.SmallIntegerField(null=True)
    date = models.SmallIntegerField(null=True)
    pages = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    def __str__(self):
        return self.title